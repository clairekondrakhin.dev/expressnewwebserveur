// import du module Express
const express = require("express");
const app = express();

// port 3000 pour accéder au serveur => http://localhost:3000/
const port = 3000;

// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
  // envoie une réponse 'Hello World!' au client
  res.send('Hello World!');
})


// création d'une nouvelle route GET sur l'URL ('/data')
app.get('/data', (req, res) => {
  // envoie une réponse de type Json au client
  res.send({ name: "Pikachu", power: 20, life: 50 });
})

// démarrage du serveur sur le port défini
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
